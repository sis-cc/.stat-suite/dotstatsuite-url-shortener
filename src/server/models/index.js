import { reduce } from 'ramda'
import Url from './urls.js'

const providers = [Url]

export default async ctx => {
  const { mongo } = ctx()

  const models = await reduce(
    async (acc, provider) => {
      const res = await acc
      const [name, model, { indexes } = {}] = await provider(mongo)
      indexes && model.ensureIndexes && (await model.ensureIndexes(indexes))
      return { ...res, [name]: model }
    },
    Promise.resolve({}),
    providers,
  )

  return models
}
