import Model from './model.js'
import { SUSError } from '../utils/errors.js'
import urlJoin from 'url-join'
// import mongodb from 'mongodb'
// const { ObjectId } = mongodb

const NAME = 'urls'

const makeShortenedUrl = (baseShortenedUrl, doc) => {
  return urlJoin(baseShortenedUrl, encodeId(doc._id))
}

const decodeId = id => {
  const _id = parseInt(id, 36)
  if (Number.isNaN(_id)) throw new SUSError(400, `Wrong id: "${id}"`)
  return _id
}

const encodeId = _id => _id.toString(36)

const out = doc => {
  const res = { ...doc, id: encodeId(doc._id) }
  delete res._id
  return res
}

export class Url extends Model {
  constructor(name, collection) {
    super(name, collection)
  }

  async insert(baseShortenedUrl, doc) {
    let retries = 0
    const loop = true
    while (loop) {
      const previous = (await this.collection.find({}, { projection: { _id: 1 } }).sort({ _id: -1 }).limit(1).toArray())[0]
      const seq = previous ? previous._id + 1 : 1
      doc._id = seq
      doc.shortenedUrl = makeShortenedUrl(baseShortenedUrl, doc)
      try {
        await this.collection.insertOne(doc)
      } catch (err) {
        if (err?.code === 11000) {
          retries++
          if (retries < 100) {
            continue
          } else {
            throw new Error('Cannot insert requested url')
          }
        } else {
          throw err
        }
      }
      return doc
    }
  }

  async createOne({ authorEmail, sourceUrl, baseShortenedUrl, ...props } = {}) {
    if (!authorEmail) throw new Error(`'authorEmail' is mandatory!`)
    if (!sourceUrl) throw new Error(`'sourceUrl' is mandatory!`)
    if (!baseShortenedUrl) throw new Error(`'baseShortenedUrl' is mandatory!`)

    const data = {
      authorEmail,
      sourceUrl,
      createdAt: new Date(),
      usageCount: 0,
    }

    if (props.appSource) data.appSource = props.appSource
    if (props.tenantId) data.tenantId = props.tenantId

    const doc = await this.insert(baseShortenedUrl, data)
    return { id: encodeId(doc._id), shortenedUrl: doc.shortenedUrl }
  }

  async findOne(id) {
    const _id = decodeId(id)
    const res = await this.collection.findOneAndUpdate({ _id }, { $set: { lastAccessedAt: new Date() }, $inc: { usageCount: 1 } }, { returnDocument: 'after' })
    if (!res.value) return
    return out(res.value)
  }

  async findMany({ authorEmail, tenantId, appSource }) {
    const filter = {}
    if (authorEmail) filter.authorEmail = authorEmail
    if (tenantId) filter.tenantId = tenantId
    if (appSource) filter.appSource = appSource

    const res = await this.collection.find(filter).toArray()
    return res.map(out)
  }
}

export default async ({ database }) => {
  const collection = database.collection(NAME)
  return [
    NAME,
    new Url(NAME, collection),
  ]
}
