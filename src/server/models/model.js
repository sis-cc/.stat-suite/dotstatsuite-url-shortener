import { reduce } from 'ramda'

export default class Model {
  constructor(name, collection) {
    this.collection = collection
    this.name = name
  }

  async ensureIndexes(indexes = []) {
    return reduce(
      (acc, [spec, options]) => acc.then(() => this.collection.createIndex(spec, options)),
      Promise.resolve(),
      indexes,
    )
  }

}
