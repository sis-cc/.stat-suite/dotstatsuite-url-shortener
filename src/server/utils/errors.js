export class HTTPError extends Error {
  constructor(code, message) {
    super(message)
    this.name = this.constructor.name
    this.code = code
  }
}


export class SUSError extends Error {
  constructor(code, message) {
    super(message)
    this.code = code
    this.name = this.constructor.name
  }
}

