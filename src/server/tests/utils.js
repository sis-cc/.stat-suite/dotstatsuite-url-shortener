import { initResources } from 'jeto'
import { v4 as uuid } from 'uuid'
import initModels from '../init/models.js'
import initMongo from '../init/mongo.js'
import initHttp from '../init/http.js'
import initRouter from '../init/router.js'
import initServices from '../services/index.js'

export const doBeforeAll = async () => {
  let CTX
  const resources = [
    initConfig,
    initMongo,
    initModels,
    initServices,
    initRouter,
    initHttp,
  ]
  try {
    await initResources(resources).then(async ctx => {
      CTX = ctx
    })
    return CTX
  } catch (err) {
    console.error(err) // eslint-disable-line
    if (CTX) {
      const { httpServer, mongo } = CTX()
      if (mongo) await mongo.dropDatabase().then(() => mongo.close())
      if (httpServer) await httpServer.close()
    }
  }
}

export const doAfterAll = async ctx => {
  const {
    mongo,
    httpServer,
    config: {
      mongo: { dbName },
    },
  } = ctx()
  return mongo
    .dropDatabase(dbName)
    .then(() => mongo.clearTestDbs())
    .then(() => mongo.close())
    .then(() => httpServer.close())
}

export const initConfig = ctx =>
  ctx({
    config: {
      httpServer: { host: '0.0.0.0' },
      mongo: {
        url: process.env.CI ? 'mongodb://mongo:27017' : 'mongodb://localhost:27017',
        dbName: `test-${uuid()}`,
      },
    },
  })
