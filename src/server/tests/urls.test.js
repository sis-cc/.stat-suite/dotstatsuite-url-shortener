import urljoin from 'url-join'
import { times, prop } from 'ramda'
import got from 'got'
import { doBeforeAll, doAfterAll } from './utils'

let CTX

describe('Urls', () => {
  const now = new Date()
  let accessedAt

  beforeAll(async () => {
    CTX = await doBeforeAll()
  })

  afterAll(() => doAfterAll(CTX))

  describe('URLS', () => {
    it('should create one', async () => {
      const { httpServer } = CTX()
      const url = urljoin(httpServer.url, `/api/urls/createOne`)
      const body = {
        authorEmail: 'author',
        sourceUrl: '/source',
        baseShortenedUrl: 'https://de/s',
      }

      const res = await got.post(url, { json: body }).json()
      expect(res.id).toEqual('1')
      expect(res.shortenedUrl).toEqual('https://de/s/1')
    })

    it('should create an other one', async () => {
      const { httpServer } = CTX()
      const url = urljoin(httpServer.url, `/api/urls/createOne`)
      const body = {
        authorEmail: 'author1',
        sourceUrl: '/source1',
        baseShortenedUrl: 'https://de/s',
        tenantId: 'tenant1',
        appSource: 'app1',
      }

      const res = await got.post(url, { json: body }).json()
      expect(res.id).toEqual('2')
    })

    it('should get second', async () => {
      const { httpServer } = CTX()
      const url = urljoin(httpServer.url, `/api/urls/findOne/2`)
      const res = await got.get(url).json()
      expect(res.id).toEqual('2')
      expect(res.authorEmail).toEqual('author1')
      expect(res.sourceUrl).toEqual('/source1')
      expect(res.tenantId).toEqual('tenant1')
      expect(res.appSource).toEqual('app1')
      expect(res.shortenedUrl).toEqual('https://de/s/2')
      expect(new Date(res.createdAt) < new Date()).toBeTruthy()
      expect(new Date(res.createdAt) >= now).toBeTruthy()
      expect(new Date(res.createdAt) < new Date(res.lastAccessedAt)).toBeTruthy()
      accessedAt = new Date(res.lastAccessedAt)
      expect(res.usageCount).toEqual(1)
    })

    it('should re-get second', async () => {
      const { httpServer } = CTX()
      const url = urljoin(httpServer.url, `/api/urls/findOne/2`)
      const res = await got.get(url).json()
      expect(res.usageCount).toEqual(2)
      expect(accessedAt < new Date(res.lastAccessedAt)).toBeTruthy()
    })

    it('should create many', async () => {
      const { httpServer } = CTX()
      const url = urljoin(httpServer.url, `/api/urls/createOne`)
      const body = {
        authorEmail: 'author2',
        sourceUrl: 'source2',
        baseShortenedUrl: '/de/s',
      }

      await Promise.all(times(() => got.post(url, { json: body }).json(), 100))
    })

    it('should get many', async () => {
      const { httpServer } = CTX()
      const url = urljoin(
        httpServer.url,
        `/api/urls/findMany`,
        '?authorEmail=author2',
        '?sourceUrl=source2',
      )

      const res = await got.get(url).json()
      expect(res.length).toEqual(100)
      expect(res.map(prop('id')).at(-1)).toEqual('2u')

    })



  })
})
