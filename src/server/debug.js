import pino from 'pino'
import pretty from 'pino-pretty'
import stackdriver from 'pino-stackdriver'

let level =
  process.env.DEBUG === 'true'
    ? 'debug'
    : process.env.NODE_ENV === 'test'
      ? 'silent'
      : process.env.LOGGING_LEVEL || 'info'

const streams = [{ stream: pretty() }]

if (process.env.LOGGING_DRIVER === 'gke') {
  const projectId = process.env.LOGGING_PROJECT_ID
  const logName = process.env.LOGGING_LOGNAME
  const stackDriverStream = stackdriver.createWriteStream({
    projectId,
    logName,
    resource: {
      type: 'global',
      labels: {
        app: 'sus',
      },
    },
  })
  streams.push({ level, stream: stackDriverStream })
}

export default pino({ name: 'sus', level }, pino.multistream(streams))
