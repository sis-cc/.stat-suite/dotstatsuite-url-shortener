import errorHandler from '../errors.js'
import { HTTPError } from '../../utils/errors.js'
import { jest } from '@jest/globals'

describe('errors middleware', () => {
  describe('when called without an error', () => {
    it('should call next callback', () => {
      const next = jest.fn()
      errorHandler(undefined, undefined, undefined, next)
      expect(next).toHaveBeenCalled()
    })
  })

  describe('when called with an error', () => {
    it('should send an error', () => {
      let sentCode
      const CODE = 42
      const res = {
        status: code => ({
          json: () => {
            sentCode = code
            return code
          },
        }),
        sendStatus: code => {
          sentCode = code
          return code
        },
      }
      errorHandler(new HTTPError(CODE), undefined, res)
      expect(sentCode).toEqual(CODE)
    })
  })
})
