import 'core-js/stable/index.js'
import 'regenerator-runtime/runtime.js'
import { initResources } from 'jeto'
import debug from './debug.js'
import initHttp from './init/http.js'
import initRouter from './init/router.js'
import initServices from './services/index.js'
import initConfig from './init/config.js'
import initMongo from './init/mongo.js'
import initModels from './init/models.js'
import cfonts from 'cfonts'

const cfontsSettings = {
  font: 'tiny',
  align: 'left',
  colors: ['cyanBright', 'white'],
  background: 'transparent',
  letterSpacing: 1,
  lineHeight: 1,
  space: true,
  maxLength: '0',
  gradient: ['yellow', 'red'],
  independentGradient: true,
  transitionGradient: true,
  env: 'node',
}

const resources = [
  initConfig,
  ctx => {
    debug.info(ctx().config, 'running config')
    return ctx
  },
  initMongo,
  initModels,
  initServices,
  initRouter,
  initHttp,
  ctx => {
    debug.info(ctx().config, 'running config')
    return ctx
  },
]

initResources(resources)
  .then(() => debug.info('🚀 server started'))
  .then(() => cfonts.say("Let's shorten", cfontsSettings))
  .catch(err => {
    debug.error(err)
    process.exit()
  })
