import initHealthcheck from './healthcheck/index.js'
import initApi from './api/index.js'
import { initResources } from '../utils/index.js'

const resources = [initHealthcheck, initApi]
export default initResources(resources)
