import { HTTPError } from '../../utils/errors.js'

const NAME = 'urls'

const service = {
  async createOne({ authorEmail, sourceUrl, appSource, tenantId, baseShortenedUrl }) {
    if (!authorEmail) throw new HTTPError(400, `'authorEmail' parameter is mandatory!`)
    if (!sourceUrl) throw new HTTPError(400, `'sourceUrl' parameter is mandatory!`)
    if (!baseShortenedUrl) throw new HTTPError(400, `'baseShortenedUrl' parameter is mandatory!`)

    const { models } = this.globals()
    return await models.urls.createOne({ authorEmail, sourceUrl, appSource, tenantId, baseShortenedUrl })
  },

  async findOne({ id }) {
    if (!id) throw new HTTPError(400, `'id' parameter is mandatory!`)
    const { models } = this.globals()
    const res = await models.urls.findOne(id)
    if (!res) throw new HTTPError(404)
    return res
  },

  async findMany({ authorEmail, tenantId, appSource }) {
    const { models } = this.globals()
    const res = await models.urls.findMany({ authorEmail, tenantId, appSource })
    if (!res || !res.length) throw new HTTPError(404)
    return res
  },


}

export default evtx =>
  evtx
    .use(NAME, service)
    .service(NAME)
