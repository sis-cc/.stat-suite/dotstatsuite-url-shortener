import evtX from 'evtx'
import debug from '../../debug.js'
import { initServices } from '../utils.js'
import initUrls from './urls.js'

const services = [initUrls]

export default ctx => {
  const api = evtX.default(ctx).configure(initServices(services))
  debug.info('api service up.')
  return ctx({ services: { ...ctx().services, api } })
}
