import Models from '../models/index.js'

export default async ctx => {
  const models = await Models(ctx)
  return ctx({ models })
}
