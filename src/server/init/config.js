import '../env.cjs'

export default ctx => {
  const httpServer = { host: process.env.SERVER_HOST || '0.0.0.0', port: Number(process.env.SERVER_PORT) || 80 }

  const config = {
    appId: 'url-shortener',
    startTime: new Date(),
    isProduction: process.env.NODE_ENV === 'production',
    gitHash: process.env.GIT_HASH || 'local',
    env: process.env.NODE_ENV,
    httpServer,
    mongo: {
      url: process.env.MONGODB_URL || 'mongodb://localhost:27017',
      dbName: process.env.MONGODB_DATABASE || 'url-shortener',
    },
  }


  return ctx({ config })
}
