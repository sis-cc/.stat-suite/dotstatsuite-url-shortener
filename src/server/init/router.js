import express from 'express'
import cors from 'cors'
import helmet from 'helmet'
import path from 'path'
import compression from 'compression'
import bodyParser from 'body-parser'
import debug from '../debug.js'
import healthcheckConnector from '../services/healthcheck/connector.js'
import apiConnector from '../services/api/connector.js'
import errorHandler from '../middlewares/errors.js'
import logger from 'express-pino-logger'

const expressPino = logger({ logger: debug })

export default async ctx => {
  const {
    services: { healthcheck, api },
  } = ctx()

  const app = express()
  app.use(cors({ exposedHeaders: ['Accept-Ranges, Content-Range'] }))
  app.use(helmet.hidePoweredBy())
  app.use(helmet.hsts())
  app.use(bodyParser.json({ limit: '10kb' }))
  app.get('/robots.txt', (req, res) => res.sendFile(path.resolve(__dirname, '../robots.txt')))
  app.use('/healthcheck', healthcheckConnector(healthcheck))
  app.use('/api', apiConnector(api))
  app.use(expressPino)
  app.use(compression())
  app.use(errorHandler)

  debug.info('HTTP routes setup')
  return ctx({ app })
}
