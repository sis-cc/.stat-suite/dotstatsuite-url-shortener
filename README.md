
## Purpose
<!---->
Allow to deep link HTTP resources with shorten URLs.

## Usage Workflow

Any user can click on a previously created shortened URL (see below to get syntax description). Resulting HTTP GET request will be intercepted by the source application backend that will identify the request as a shortener one, extract the id, call the shortener URL server (aka SUS) to get the associated URL, and redirect the previous request to it.

## URL Shortener Data Model

```
    id
    authorEmail
    sourceURL
    shortenedUrl
    createdAt
    lastAccessedAt
    usageCount
    appSource
    tenantId
```


## API

NB: All requests can be used with GET or POST verbs

### POST /api/urls/createOne

body:

```
{
    authorEmail: (mandatory),
    sourceURL: (mandatory),
    baseShortedUrl: base shortened url to compute `shortenedUrl` id (mandatory) 
    tenantId,
    appSource,
}
```

*returns* `{ id }`

### GET /api/urls/findOne/:id

returns full data linked to `id`

### GET /api/urls/findMany

params:

```
{
    authorEmail,
    tenantId,
    appSource,
}
```

returns all data filtered by those params

### example

let's get `http://localhost:7104/api/urls/createOne?authorEmail=toto&sourceUrl=http://google.com&baseShortenedUrl=http://de.org`

Request will return `{ id: '2v' }`

let's request `http://localhost:7104/api/urls/findOne/2v`

we will get:

```
    id: "2v"
    authorEmail: "toto",
    sourceUrl: "http://google.com",
    shortenedUrl: "http://de.org/2v",
    usageCount: 1,
    createdAt: "2024-04-30T14:36:05.669Z",
    lastAccessedAt: "2024-04-30T15:00:09.175Z",
```


