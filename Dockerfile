FROM node:20-alpine

ARG GIT_HASH

RUN mkdir -p /sus
WORKDIR /sus

RUN echo $GIT_HASH

COPY package.json yarn.lock /sus/
COPY src /sus/src

RUN yarn install --production && yarn cache clean

ENV GIT_HASH=$GIT_HASH

EXPOSE 80
CMD yarn dist:run
